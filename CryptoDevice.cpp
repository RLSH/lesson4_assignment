#include "CryptoDevice.h"

//aquire a default key for AES and block size for CBC. can't generate randomly, since all the agents shuold have the same key.
//can initialize with a unique sequence but not needed.

//this function encrypt the message
std::string CryptoDevice::encryptAES(std::string plainText)
{
	std::string cipherText;
	int i = 0;
	int key = 4;

	for (i = 0; i < plainText.length(); i++)
	{
		cipherText += plainText[i] + key;
	}
	

	return cipherText;
}

//this function decrypt the message
std::string CryptoDevice::decryptAES(std::string cipherText)
{
    std::string decryptedText;
	int i = 0;
	int key = 4;

	for (i = 0; i < cipherText.length(); i++)
	{
		decryptedText += cipherText[i] - key;
	}

	return decryptedText;
}
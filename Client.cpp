#include "Client.h"
#include <cstdlib>
#include <thread>
#include <md5.h>
#include <hex.h>

#define CRYPTOPP_ENABLE_NAMESPACE_WEAK 1

CryptoDevice cryptoDevice;

int process_client(client_type &new_client)
{
	CryptoDevice temp;

	while (1)
	{
		std::memset(new_client.received_message, 0, DEFAULT_BUFLEN);

		if (new_client.socket != 0)
		{


			int iResult = recv(new_client.socket, new_client.received_message, DEFAULT_BUFLEN, 0);

            std::string strMessage(new_client.received_message);

			// some logic, we dont want to decrypt notifications sent by the operator
			// our protocol says - ": " means notification from the operator
			size_t position = strMessage.find(": ") + 2;
            std::string prefix = strMessage.substr(0, position);
            std::string postfix = strMessage.substr(position);
            std::string decrypted_message;

			//this is the only notification we use right now :(
            if (postfix != "Disconnected") {
                //please decrypt this part!
                decrypted_message = temp.decryptAES(postfix);
            }
            else 
            {
                //dont decrypt this - not classified and has not been ecrypted! 
                //trying to do so may cause errors
                decrypted_message = postfix;
            }

            if (iResult != SOCKET_ERROR) 
            {
                std::cout << prefix + postfix << std::endl;
            }            
			else
			{
                std::cout << "recv() failed: " << ::WSAGetLastError() << std::endl;
				break;
			}
		}
	}

	if (::WSAGetLastError() == WSAECONNRESET)
        std::cout << "The server has disconnected" << std::endl;

	return 0;
}

bool passfunc(std::string message)
{
	//to get the hash code i use this code but i deleted the password
	//user: harelharel7
	//pass: magshimim
	std::string userpass = "4B7CC6DCDBD2A5E3D3C5D8ED9FAC1750";
	byte digest[CryptoPP::Weak1::MD5::DIGESTSIZE];
	CryptoPP::Weak1::MD5 hash;
	hash.CalculateDigest(digest, (const byte*)message.c_str(), message.length());

	CryptoPP::HexEncoder encoder;
	std::string output;
	
	encoder.Attach(new CryptoPP::StringSink(output));
	encoder.Put(digest, sizeof(digest));
	encoder.MessageEnd();

	return (userpass == output);
}

int main()
{
	int i = 0;
	std::string str = "";
	bool checkthis = false;
	bool ans = false;

	//this check the password and the username
	for (i = 0; i < 5; i++)
	{
		std::cout << "enter user name and password int this format: [user][pass]" << std::endl;
		std::getline(std::cin, str);
		ans = passfunc(str);

		if (ans)
		{
			std::cout << "correct user and pass!" << std::endl;
			checkthis = true;
			break;
		}
		else
		{
			std::cout << "wrong user and pass:(" << std::endl;
			std::cout << "you have " << 5 - i -1<< " tries left!!! \ntry again \n\n" << std::endl;
			checkthis = false;
		}
	}

	//if the password succesed continue the code
	if (checkthis) 
	{
		WSAData wsa_data;
		struct addrinfo *result = NULL, *ptr = NULL, hints;
		std::string sent_message = "";
		client_type client = { INVALID_SOCKET, -1, "" };
		int iResult = 0;
		std::string message;
		CryptoDevice temp;

		std::cout << "Starting Client...\n";

		// Initialize Winsock
		iResult = ::WSAStartup(MAKEWORD(2, 2), &wsa_data);
		if (iResult != 0) {
			std::cout << "WSAStartup() failed with error: " << iResult << std::endl;
			return 1;
		}

		ZeroMemory(&hints, sizeof(hints));
		hints.ai_family = AF_UNSPEC;
		hints.ai_socktype = SOCK_STREAM;
		hints.ai_protocol = IPPROTO_TCP;

		std::cout << "Connecting...\n";

		// Resolve the server address and port
		iResult = getaddrinfo(IP_ADDRESS, DEFAULT_PORT, &hints, &result);
		if (iResult != 0) {
			std::cout << "getaddrinfo() failed with error: " << iResult << std::endl;
			::WSACleanup();
			std::system("pause");
			return 1;
		}

		// Attempt to connect to an address until one succeeds
		for (ptr = result; ptr != NULL; ptr = ptr->ai_next) {

			// Create a SOCKET for connecting to server
			client.socket = socket(ptr->ai_family, ptr->ai_socktype,
				ptr->ai_protocol);
			if (client.socket == INVALID_SOCKET) {
				std::cout << "socket() failed with error: " << ::WSAGetLastError() << std::endl;
				::WSACleanup();
				std::system("pause");
				return 1;
			}

			// Connect to server.
			iResult = ::connect(client.socket, ptr->ai_addr, (int)ptr->ai_addrlen);
			if (iResult == SOCKET_ERROR) {
				closesocket(client.socket);
				client.socket = INVALID_SOCKET;
				continue;
			}
			break;
		}

		freeaddrinfo(result);

		if (client.socket == INVALID_SOCKET) {
			std::cout << "Unable to connect to server!" << std::endl;
			::WSACleanup();
			std::system("pause");
			return 1;
		}

		std::cout << "Successfully Connected" << std::endl;

		//Obtain id from server for this client;
		::recv(client.socket, client.received_message, DEFAULT_BUFLEN, 0);
		message = client.received_message;

		if (message != "Server is full")
		{
			client.id = std::atoi(client.received_message);

			std::thread my_thread(process_client, std::ref(client));

			while (1)
			{
				std::getline(std::cin, sent_message);

				//top secret! please encrypt
				std::string cipher = temp.encryptAES(sent_message);

				iResult = ::send(client.socket, cipher.c_str(), cipher.length(), 0);

				if (iResult <= 0)
				{
					std::cout << "send() failed: " << ::WSAGetLastError() << std::endl;
					break;
				}
			}

			//Shutdown the connection since no more data will be sent
			my_thread.detach();
		}
		else
			std::cout << client.received_message << std::endl;

		std::cout << "Shutting down socket..." << std::endl;
		iResult = ::shutdown(client.socket, SD_SEND);
		if (iResult == SOCKET_ERROR) {
			std::cout << "shutdown() failed with error: " << ::WSAGetLastError() << std::endl;
			::closesocket(client.socket);
			::WSACleanup();
			std::system("pause");
			return 1;
		}

		::closesocket(client.socket);
		::WSACleanup();
		std::system("pause");
		return 0;
	}

	return 1;
}